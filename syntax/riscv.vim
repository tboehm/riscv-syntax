" Vim syntax file for RISC-V assembly
" Author: Trey Boehm
" Date: 2020-05-21

if exists("b:current_syntax")
    finish
endif

syn case ignore

" ------------------------------ Numbers -------------------------------
syn match riscvDecimal /-\?[0-9]\+/
syn match riscvHex     /-\?0x[A-Fa-f0-9]\+/
syn match riscvBinary  /-\?0b[01]\+/

" ----------------------------- Delimiters ------------------------------
syn match riscvDelimiter "[,:()]"

" ------------------------------ Strings -------------------------------
" Strings are surrounded by double quotes
syn region riscvString start=+"+ skip=+\\"+ end=+"+

" ----------------------------- Comments -------------------------------
" Comments begin with a hash
syn region riscvComment start=+#+ end=+$+

" ----------------------------- Registers ------------------------------
" Standard register names x0-x31
syn match   riscvRegister /x\(1[0-9]\|2[0-9]\|3[0-1]\|[0-9]\)/
" ABI register names
syn keyword riscvRegister zero ra sp gp tp fp
syn match   riscvRegister /a[1-7]/
syn match   riscvRegister /s\([0-9]\|1[0-1]\)/
syn match   riscvRegister /t[0-6]/
" Standard floating-point register names f0-f31
syn match   riscvRegister /f\(1[0-9]\|2[0-9]\|3[0-1]\|[0-9]\)/
" ABI floating-point register names
syn match   riscvRegister /fa[0-7]/
syn match   riscvRegister /fs\([0-9]\|1[0-1]\)/
syn match   riscvRegister /ft\([0-9]\|1[0-1]\)/

" ------------------ Assembler relocatable functions -------------------
syn match riscvReloc /%\(hi\|lo\|pcrel_hi\|pcrel_lo\|tprel_hi\|tprel_lo
            \\|tprel_add\|tls_ie_pcrel_hi\|tls_gd_pcrel_hi
            \\|got_pcrel_hi\)/

" ------------------------------ Opcodes -------------------------------
" RV32I
syn keyword riscvOpcode lui auipc jal jalr beq bne blt bge bltu bgeu lb
            \ lh lw lbu lhu sb sh sw addi slti sltiu xori ori andi slli
            \ srli srai add sub sll slt sltu xor srl sra or and fence.i
            \ fence ecall ebreak csrrw csrrs csrrc csrrwi csrrsi csrrci

" RV64I
syn keyword riscvOpcode lwu ld sd slli srli srai addiw slliw srliw sraiw
            \ addw subw sllw srlw sraw

" RV32M
syn keyword riscvOpcode mul mulh mulhsu mulhu div divu rem remu

" RV64M
syn keyword riscvOpcode mulw divw divuw remw remuw

" RV32A
syn keyword riscvOpcode lr.w sc.w amoswap.w amoadd.w amoxor.w amoand.w
            \ amoor.w amomin.w amomax.w amominu.w amomaxu.w

" RV64A
syn keyword riscvOpcode lr.d sc.d amoswap.d amoadd.d amoxor.d amoand.d
            \ amoor.d amomin.d amomax.d amominu.d amomaxu.d

" RV32F
syn keyword riscvOpcode flw fsw fmadd.s fmsub.s fnmsub.s fnmadd.s fadd.s
            \ fsub.s fmul.s fdiv.s fsqrt.s fsgnj.s fsgnjn.s fsgnjx.s
            \ fmin.s fmax.s fcvt.w.s fcvt.wu.s fmv.x.w feq.s flt.s fle.s
            \ fclass.s fcvt.s.w fcvt.s.wu fmv.w.x

" RV64F
syn keyword riscvOpcode fcvt.l.s fcvt.lu.s fcvt.s.l fcvt.s.lu

" RV32D
syn keyword riscvOpcode fld fsd fmadd.d fmsub.d fnmsub.d fnmadd.d fadd.d
            \ fsub.d fmul.d fdiv.d fsqrt.d fsgnj.d fsgnjn.d fsgnjx.d
            \ fmin.d fmax.d fcvt.s.d fcvt.d.s feq.d flt.d fle.d fclass.d
            \ fcvt.w.d fcvt.wu.d fcvt.d.w fcvt.d.wu

" RV64D
syn keyword riscvOpcode fcvt.l.d fcvt.lu.d fmv.x.d fcvt.d.l fcvt.d.lu
            \ fmv.d.x

" RV32Q
syn keyword riscvOpcode flq fsq fmadd.q fmsub.q fnmsub.q fnmadd.q fadd.q
            \ fsub.q fmul.q fdiv.q fsqrt.q fsgnj.q fsgnjn.q fsgnjx.q
            \ fmin.q fmax.q fcvt.s.q fcvt.q.s fcvt.d.q fcvt.q.d feq.q
            \ flt.q fle.q fclass.q fcvt.w.q fcvt.wu.q fcvt.q.w fcvt.q.wu

" RV64Q
syn keyword riscvOpcode fcvt.l.q fcvt.lu.q fcvt.q.l fcvt.q.lu

" ------------------------ Pseudo-instructions -------------------------
syn keyword riscvPseudo la lla nop li mv not neg negw sext.w seqz snez
            \ sltz sgtz fmv.s fabs.s fneg.s fmv.d fabs.d fneg.d beqz
            \ bnez blez bgez bltz bgtz bgt ble bgtu bleu j jr ret call
            \ tail rdinstret rdinstreth rdcycle rdcycleh rdtime rdtimeh
            \ csrr csrw csrs csrc csrwi csrsi csrci frcsr fscsr fscsr
            \ frrm fsrm fsrm frflags fsflags fsflags

" ---------------------------- Directives ------------------------------
"syn keyword riscvDirective .align .file .globl .local .comm .common
"            \ .ident .section .size .text .data .rodata .bss .string
"            \ .asciz .equ .macro .endm .type .option .byte .2byte .half
"            \ .short .4byte .word .long .8byte .dword .quad .dtprelword
"            \ .dtpreldword .sleb128 .uleb128 .p2align .balign .zero
"            \ .fill
syn match riscvDirective /\.\(align\|file\|globl\|local\|comm\|common
            \\|ident\|section\|size\|text\|data\|rodata\|bss\|string
            \\|asciz\|equ\|macro\|endm\|type\|option\|byte\|2byte\|half
            \\|short\|4byte\|word\|long\|8byte\|dword\|quad\|dtprelword
            \\|dtpreldword\|sleb128\|uleb128\|p2align\|balign\|zero
            \\|fill\)/

" ------------------------------- Labels -------------------------------
" Local references (don't match the : at the end)
syn match riscvLocalRef /\([1-9]\ze:\|[1-9]b\|[1-9]f\)/
" Regular labels. These are usually lowercase, so to avoid conflicts
" with matching macros I will not match uppercase starting letter.
syn case match
syn match riscvLabel    /[_a-z][_a-zA-Z0-9]\+/
syn case ignore

" ----------------------- Pre-processor macros -------------------------
" Match everything up until an opening parenthesis. Notice: This will
" only work on the macros that take arguments.
syn match riscvMacro /[_a-zA-Z][_a-zA-Z0-9]\+\ze(/
" However, most (stylistically correct) macros have no lowercase
" characters. Enforce starting with a capital letter or underscore.
syn case match
syn match riscvMacro /[_A-Z][_a-zA-Z0-9]\+/
syn case ignore

" --------------------- C preprocessor statements ----------------------
" This snippet is from https://github.com/Shirk/vim-gas
" Copyright (c) 2009-2018, RenéKöcher <info@bitspin.org>
syn case match

syn include @cPP syntax/c.vim
syn match   cPPLineCont "\\$" contained

syn region cPPPreProc start=/^\s*#\s*\(if\|else\|endif\|define\|include\)/
            \ end=/$/ contains=@cPP,cPPLineCont
syn region riscvComment start=+\/\/+ end=+$+
" (end of the snippet from vim-gas)

" Link colors.
hi def link riscvDecimal   Number
hi def link riscvHex       Number
hi def link riscvBinary    Number
hi def link riscvDelimiter None
hi def link riscvString    String
hi def link riscvComment   Comment
hi def link riscvReloc     Keyword
hi def link riscvRegister  Type
hi def link riscvOpcode    Keyword
hi def link riscvPseudo    Keyword
hi def link riscvDirective Keyword
hi def link riscvLocalRef  Label
hi def link riscvLabel     Label
hi def link riscvMacro     Macro

let b:current_syntax = "riscv"
