riscv-syntax
============

Syntax highlighting for RISC-V assembly in vim.

The [vim-gas](https://github.com/Shirk/vim-gas) plugin does not provide syntax
highlighting for RISC-V, so I pieced this together. Some of the choices rely on
consistent programming style (for example, expecting that CPP macros start with
a capital letter or underscore).
